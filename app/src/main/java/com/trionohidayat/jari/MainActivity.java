package com.trionohidayat.jari;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.trionohidayat.jari.soal1.Soal1Activity;
import com.trionohidayat.jari.soal2.Soal2Activity;
import com.trionohidayat.jari.soal3.Soal3Activity;
import com.trionohidayat.jari.soal4.Soal4Activity;
import com.trionohidayat.jari.soal5.Soal5Activity;
import com.trionohidayat.jari.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_WRITE_EXTERNAL_STORAGE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.buttonSoal1.setOnClickListener(view -> startActivity(new Intent(this, Soal1Activity.class)));
        binding.buttonSoal2.setOnClickListener(view -> startActivity(new Intent(this, Soal2Activity.class)));
        binding.buttonSoal3.setOnClickListener(view -> startActivity(new Intent(this, Soal3Activity.class)));
        binding.buttonSoal4.setOnClickListener(view -> startActivity(new Intent(this, Soal4Activity.class)));
        binding.buttonSoal5.setOnClickListener(view -> {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                startActivity(new Intent(this, Soal5Activity.class));
            } else {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_EXTERNAL_STORAGE);
            }
        });
    }
}