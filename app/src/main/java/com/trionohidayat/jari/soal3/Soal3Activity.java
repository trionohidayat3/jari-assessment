package com.trionohidayat.jari.soal3;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.appbar.MaterialToolbar;
import com.trionohidayat.jari.databinding.ActivitySoal3Binding;

import java.util.Objects;

public class Soal3Activity extends AppCompatActivity {

    private ActivitySoal3Binding binding;

    MaterialToolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySoal3Binding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        toolbar = binding.toolBar;

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        binding.inputText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
                if (text.contains(" ")) {
                    text = text.replaceAll(" ", "");
                    binding.inputText.setText(text);
                    binding.inputText.setSelection(text.length());
                }
            }
        });

        binding.buttonGenerate.setOnClickListener(view -> {
            String inputString = Objects.requireNonNull(binding.inputText.getText()).toString();
            int maxLength = findMaxLengthOfUniqueSubstring(inputString);

            binding.textGenerate.setText(String.valueOf(maxLength));
            binding.inputText.setText("");
        });
    }

    private int findMaxLengthOfUniqueSubstring(String s) {
        int maxLength = 0;
        int[] charIndex = new int[256];
        int start = 0;

        for (int end = 0; end < s.length(); end++) {
            char ch = s.charAt(end);
            if (charIndex[ch] > start) {
                start = charIndex[ch];
            }
            charIndex[ch] = end + 1;
            maxLength = Math.max(maxLength, end - start + 1);
        }

        return maxLength;
    }
}