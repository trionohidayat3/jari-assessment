package com.trionohidayat.jari.soal2;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.appbar.MaterialToolbar;
import com.trionohidayat.jari.databinding.ActivitySoal2Binding;

import java.util.ArrayList;

public class Soal2Activity extends AppCompatActivity {

    private ActivitySoal2Binding binding;

    MaterialToolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySoal2Binding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        toolbar = binding.toolBar;

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        binding.buttonGenerate.setOnClickListener(view -> {
            String inputText = binding.inputNumber.getText().toString();
            if (inputText.isEmpty()) {
                Toast.makeText(this, "Mohon masukkan Angka", Toast.LENGTH_SHORT).show();
            } else {
                int x = Integer.parseInt(inputText);

                ArrayList<Integer> fibonacciList = generateFibonacci(x);
                StringBuilder result = new StringBuilder();

                for (int i = 0; i < fibonacciList.size(); i++) {
                    result.append(fibonacciList.get(i));

                    if (i < fibonacciList.size() - 1) {
                        result.append(", ");
                    }
                }

                binding.textGenerate.setText(result.toString());
            }
        });
    }

    private ArrayList<Integer> generateFibonacci(int x) {
        ArrayList<Integer> fibonacciList = new ArrayList<>();

        if (x >= 1) {
            fibonacciList.add(0);
        }
        if (x >= 2) {
            fibonacciList.add(1);
        }

        for (int i = 2; i < x; i++) {
            int nextFibonacci = fibonacciList.get(i - 1) + fibonacciList.get(i - 2);
            fibonacciList.add(nextFibonacci);
        }

        return fibonacciList;
    }
}