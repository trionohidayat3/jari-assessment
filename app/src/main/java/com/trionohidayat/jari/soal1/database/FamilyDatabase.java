package com.trionohidayat.jari.soal1.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class FamilyDatabase extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "familydb.db";
    private static final String TABLE_NAME = "tbl_family";
    private static final String COL_CODE = "code";
    private static final String COL_NAME = "name";
    private static final String COL_PARENT = "parent";
    private static final int DATABASE_VERSION = 1;

    public FamilyDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createFamilyTable(db);
        insertInitialData(db);
    }

    private void createFamilyTable(SQLiteDatabase db) {
        String createTableQuery = "CREATE TABLE " + TABLE_NAME + "(" +
                COL_CODE + " TEXT PRIMARY KEY," +
                COL_NAME + " TEXT," +
                COL_PARENT + " TEXT" +
                ");";
        db.execSQL(createTableQuery);
    }

    private void insertInitialData(SQLiteDatabase db) {
        // Insert data ke dalam tabel
        insertFamilyMember(db, "A001", "Wati", "");
        insertFamilyMember(db, "A002", "Wira", "A001");
        insertFamilyMember(db, "A003", "Andi", "A002");
        insertFamilyMember(db, "A004", "Bagus", "A002");
        insertFamilyMember(db, "A005", "Siti", "A001");
        insertFamilyMember(db, "A006", "Hasan", "A005");
        insertFamilyMember(db, "A007", "Abdul", "A006");
    }

    private void insertFamilyMember(SQLiteDatabase db, String code, String name, String parent) {
        ContentValues values = new ContentValues();
        values.put(COL_CODE, code);
        values.put(COL_NAME, name);
        values.put(COL_PARENT, parent);
        db.insert(TABLE_NAME, null, values);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Implementasikan jika Anda perlu mengubah skema basis data
    }

    public List<String> getAllFamilyCodes() {
        List<String> familyCodes = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(TABLE_NAME, new String[]{COL_CODE}, null, null, null, null, null);

        while (cursor.moveToNext()) {
            @SuppressLint("Range") String code = cursor.getString(cursor.getColumnIndex(COL_CODE));
            familyCodes.add(code);
        }

        cursor.close();
        db.close();

        return familyCodes;
    }

    public List<String> getChildrenNames(String code) {
        List<String> childrenNames = new ArrayList<>();
        getChildrenNamesRecursive(code, childrenNames);
        return childrenNames;
    }

    private void getChildrenNamesRecursive(String code, List<String> childrenNames) {
        SQLiteDatabase db = getReadableDatabase();
        String selection = FamilyDatabase.COL_PARENT + " = ?";
        String[] selectionArgs = {code};

        Cursor cursor = db.query(FamilyDatabase.TABLE_NAME, null, selection, selectionArgs, null, null, null);

        while (cursor.moveToNext()) {
            @SuppressLint("Range") String childName = cursor.getString(cursor.getColumnIndex(FamilyDatabase.COL_NAME));
            childrenNames.add(childName);

            @SuppressLint("Range") String childCode = cursor.getString(cursor.getColumnIndex(FamilyDatabase.COL_CODE));
            getChildrenNamesRecursive(childCode, childrenNames);
        }

        cursor.close();
        db.close();
    }
}

