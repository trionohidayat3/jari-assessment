package com.trionohidayat.jari.soal1;


import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.appbar.MaterialToolbar;
import com.trionohidayat.jari.soal1.database.FamilyDatabase;
import com.trionohidayat.jari.databinding.ActivitySoal1Binding;
import com.trionohidayat.jari.soal1.preferences.AppPreferences;

import java.util.List;

public class Soal1Activity extends AppCompatActivity {

    private ActivitySoal1Binding binding;

    MaterialToolbar toolbar;
    private FamilyDatabase dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySoal1Binding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        toolbar = binding.toolBar;

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        AppPreferences appPreferences = new AppPreferences(this);

        dbHelper = new FamilyDatabase(this);

        if (!appPreferences.isDatabaseCreated()) {
            dbHelper.getWritableDatabase();
            appPreferences.setDatabaseCreatedStatus(true);
            Toast.makeText(this, "Database telah dibuat dan diinisialisasi", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Database sudah ada", Toast.LENGTH_SHORT).show();
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1,
                dbHelper.getAllFamilyCodes());
        binding.autoCodeFamily.setAdapter(adapter);

        binding.autoCodeFamily.setOnItemClickListener((adapterView, view, i, l) -> {
            String inputCode = binding.autoCodeFamily.getText().toString();
            displayChildrenNames(inputCode);
        });
    }

    private void displayChildrenNames(String code) {
        StringBuilder builder = new StringBuilder();

        List<String> childrenNames = dbHelper.getChildrenNames(code);
        for (String childName : childrenNames) {
            builder.append(childName).append("\n");
        }

        binding.resultTextView.setText(builder.toString());
    }

}