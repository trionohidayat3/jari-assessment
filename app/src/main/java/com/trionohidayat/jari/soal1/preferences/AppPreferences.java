package com.trionohidayat.jari.soal1.preferences;

import android.content.Context;
import android.content.SharedPreferences;

public class AppPreferences {
    private static final String PREFS_NAME = "MyPrefsFile";
    private static final String DATABASE_CREATED_KEY = "databaseCreated";

    private final SharedPreferences preferences;

    public AppPreferences(Context context) {
        preferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public boolean isDatabaseCreated() {
        return preferences.getBoolean(DATABASE_CREATED_KEY, false);
    }

    public void setDatabaseCreatedStatus(boolean created) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(DATABASE_CREATED_KEY, created);
        editor.apply();
    }
}

