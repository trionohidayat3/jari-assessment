package com.trionohidayat.jari.soal5;

import com.trionohidayat.jari.soal5.model.Pokemon;

public interface ItemClickListener {
    void onItemClick(Pokemon pokemon);
}

