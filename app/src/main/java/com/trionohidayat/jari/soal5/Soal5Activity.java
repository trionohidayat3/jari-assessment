package com.trionohidayat.jari.soal5;

import android.app.AlertDialog;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.search.SearchView;
import com.trionohidayat.jari.R;
import com.trionohidayat.jari.databinding.ActivitySoal5Binding;
import com.trionohidayat.jari.soal5.adapter.PokemonAdapter;
import com.trionohidayat.jari.soal5.adapter.SearchAdapter;
import com.trionohidayat.jari.soal5.api.ApiClient;
import com.trionohidayat.jari.soal5.api.ApiService;
import com.trionohidayat.jari.soal5.database.PokemonDatabase;
import com.trionohidayat.jari.soal5.helper.ProgressDialogHelper;
import com.trionohidayat.jari.soal5.model.Pokemon;
import com.trionohidayat.jari.soal5.response.CardResponse;
import com.trionohidayat.jari.soal5.response.PokemonResponse;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Soal5Activity extends AppCompatActivity implements NetworkChangeReceiver.NetworkChangeListener {

    ActivitySoal5Binding binding;
    MaterialToolbar toolbar;
    private SearchView searchView;
    ApiService apiService;
    RecyclerView recyclerMain, recyclerSearch;
    TextView textOffline;
    PokemonAdapter pokemonAdapter;
    SearchAdapter searchAdapter;
    private final List<Pokemon> cards = new ArrayList<>();
    private final List<Pokemon> searchResults = new ArrayList<>();
    private int currentPage = 1;
    private ImageDownloader imageDownloader;
    private PokemonDatabase database;
    private boolean isLoading = false;
    int pageSize = 20;
    private ProgressDialogHelper progressDialogHelper;
    private NetworkChangeReceiver networkChangeReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySoal5Binding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        progressDialogHelper = new ProgressDialogHelper(this);

        database = new PokemonDatabase(this);

        networkChangeReceiver = new NetworkChangeReceiver(this);
        registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        apiService = ApiClient.getClient().create(ApiService.class);
        imageDownloader = new ImageDownloader(this);

        toolbar = binding.toolBar;
        searchView = binding.searchView;
        recyclerSearch = binding.recyclerSearch;
        textOffline = binding.textOffline;
        recyclerMain = binding.recyclerMain;

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        searchView.getEditText().setOnEditorActionListener((textView, i, keyEvent) -> {
            performSearch(searchView.getEditText().getText().toString());
            return false;
        });

        searchAdapter = new SearchAdapter(searchResults, this, networkChangeReceiver.isConnected(this));
        recyclerSearch.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerSearch.setAdapter(searchAdapter);

        recyclerSearch.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerMain, int dx, int dy) {
                super.onScrolled(recyclerMain, dx, dy);

                LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerMain.getLayoutManager();
                assert layoutManager != null;
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (!isLoading && (visibleItemCount + firstVisibleItemPosition) >= totalItemCount) {
                    loadNextPageSearch();
                }
            }
        });

        recyclerMain.setLayoutManager(new GridLayoutManager(this, 3));
        pokemonAdapter = new PokemonAdapter(cards, this, networkChangeReceiver.isConnected(this));
        recyclerMain.setAdapter(pokemonAdapter);

        if (networkChangeReceiver.isConnected(this)) {
            fetchDataFromJson();
        } else {
            displayPokemonDataFromSQLite();
        }

        pokemonAdapter.setItemClickListener(pokemon -> {
            if (networkChangeReceiver.isConnected(this)){
                displayPokemonDetails(pokemon);
            } else {
                Toast.makeText(this, "Detail hanya bisa mode Online", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.actionSearch) {
            searchView.show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(networkChangeReceiver);
    }

    @Override
    public void onNetworkChanged(boolean isConnected) {
        if (isConnected) {
            fetchDataFromJson();
        } else {
            displayPokemonDataFromSQLite();
        }
    }

    private void fetchDataFromJson() {
        binding.textOffline.setVisibility(View.GONE);
        progressDialogHelper.show();
        Call<PokemonResponse> call = apiService.searchCards("", 1, pageSize);
        call.enqueue(new Callback<PokemonResponse>() {
            @Override
            public void onResponse(@NonNull Call<PokemonResponse> call, @NonNull Response<PokemonResponse> response) {
                isLoading = false;

                if (response.isSuccessful()) {
                    progressDialogHelper.hide();
                    PokemonResponse pokemonResponse = response.body();
                    if (pokemonResponse != null) {
                        List<Pokemon> cards = pokemonResponse.getData();

                        for (Pokemon pokemon : cards) {
                            downloadAndSavePokemonImage(pokemon.getImages().getSmall(), pokemon.getId(), pokemon.getName());
                        }

                        pokemonAdapter.setCards(cards);
                        pokemonAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<PokemonResponse> call, @NonNull Throwable t) {
                progressDialogHelper.hide();
                isLoading = false;
                Toast.makeText(Soal5Activity.this, "Gagal mengambil data dari API.", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });

        recyclerMain.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerMain, int dx, int dy) {
                super.onScrolled(recyclerMain, dx, dy);

                LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerMain.getLayoutManager();
                assert layoutManager != null;
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (!isLoading && (visibleItemCount + firstVisibleItemPosition) >= totalItemCount) {
                    loadNextPageMain();
                }
            }
        });


    }

    private void displayPokemonDataFromSQLite() {
        binding.textOffline.setVisibility(View.VISIBLE);
        List<Pokemon> pokemonList = database.loadPokemonDataFromSQLite();

        if (!pokemonList.isEmpty()) {
            pokemonAdapter.setCards(pokemonList);
        }
    }

    private void downloadAndSavePokemonImage(String imageUrl, String pokemonId, String pokemonName) {
        imageDownloader.downloadAndSaveImage(imageUrl, pokemonId, pokemonName);
    }

    private void performSearch(String query) {
        progressDialogHelper.show();

        Call<PokemonResponse> call = apiService.searchCards("name:" + query, 1, pageSize);
        call.enqueue(new Callback<PokemonResponse>() {
            @Override
            public void onResponse(@NonNull Call<PokemonResponse> call, @NonNull Response<PokemonResponse> response) {
                isLoading = false;

                if (response.isSuccessful()) {
                    progressDialogHelper.hide();
                    PokemonResponse pokemonResponse = response.body();
                    if (pokemonResponse != null) {
                        List<Pokemon> searchResults = pokemonResponse.getData();
                        showSearchResults(searchResults);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<PokemonResponse> call, @NonNull Throwable t) {
                progressDialogHelper.hide();
                isLoading = false;
                t.printStackTrace();
            }
        });
    }

    private void showSearchResults(List<Pokemon> results) {
        searchResults.clear();
        searchResults.addAll(results);
        searchAdapter.notifyDataSetChanged();
        recyclerSearch.setVisibility(View.VISIBLE);
    }

    private void loadNextPageMain() {
        progressDialogHelper.show();
        currentPage++;
        isLoading = true;


        Call<PokemonResponse> call = apiService.searchCards("", currentPage, 20);
        call.enqueue(new Callback<PokemonResponse>() {
            @Override
            public void onResponse(@NonNull Call<PokemonResponse> call, @NonNull Response<PokemonResponse> response) {
                isLoading = false;

                if (response.isSuccessful()) {
                    progressDialogHelper.hide();
                    PokemonResponse pokemonResponse = response.body();
                    if (pokemonResponse != null) {
                        List<Pokemon> newCards = pokemonResponse.getData();
                        pokemonAdapter.addCards(newCards);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<PokemonResponse> call, @NonNull Throwable t) {
                progressDialogHelper.hide();
                isLoading = false;
                t.printStackTrace();
            }
        });
    }

    private void loadNextPageSearch() {
        progressDialogHelper.show();
        currentPage++;
        isLoading = true;

        Call<PokemonResponse> call = apiService.searchCards("", currentPage, 20);
        call.enqueue(new Callback<PokemonResponse>() {
            @Override
            public void onResponse(@NonNull Call<PokemonResponse> call, @NonNull Response<PokemonResponse> response) {
                isLoading = false;

                if (response.isSuccessful()) {
                    progressDialogHelper.hide();
                    PokemonResponse pokemonResponse = response.body();
                    if (pokemonResponse != null) {
                        List<Pokemon> newCards = pokemonResponse.getData();
                        pokemonAdapter.addCards(newCards);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<PokemonResponse> call, @NonNull Throwable t) {
                progressDialogHelper.hide();
                isLoading = false;
                t.printStackTrace();
            }
        });
    }

    private void displayPokemonDetails(Pokemon pokemon) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_pokemon_info, null);
        builder.setView(dialogView);

        ImageView imagePokemon = dialogView.findViewById(R.id.imagePokemon);
        TextView textName, textType, textDamage, textAttackName, textSuperType, textEvolve, textHP;
        textName = dialogView.findViewById(R.id.textName);
        textType = dialogView.findViewById(R.id.textType);
        textDamage = dialogView.findViewById(R.id.textDamage);
        textAttackName = dialogView.findViewById(R.id.textAttackName);
        textSuperType = dialogView.findViewById(R.id.textSuperType);
        textEvolve = dialogView.findViewById(R.id.textEvolve);
        textHP = dialogView.findViewById(R.id.textHP);

        Call<CardResponse> call = apiService.getCards(pokemon.getId());
        call.enqueue(new Callback<CardResponse>() {
            @Override
            public void onResponse(@NonNull Call<CardResponse> call, @NonNull Response<CardResponse> response) {
                if (response.isSuccessful()) {
                    CardResponse cardResponse = response.body();
                    if (cardResponse != null) {

                        textName.setText(cardResponse.getData().getName());
                        textDamage.setText(cardResponse.getData().getAttacks().get(0).getDamage());
                        textHP.setText(cardResponse.getData().getHp());
                        textEvolve.setText(cardResponse.getData().getEvolvesFrom());
                        textAttackName.setText(cardResponse.getData().getAttacks().get(0).getName());
                        textSuperType.setText(cardResponse.getData().getSupertype());

                        List<String> types = cardResponse.getData().getTypes();
                        if (types != null && !types.isEmpty()) {
                            StringBuilder typesText = new StringBuilder();
                            for (String type : types) {
                                typesText.append(type).append(", ");
                            }
                            typesText.setLength(typesText.length() - 2);
                            textType.setText(typesText.toString());
                        } else {
                            textType.setText("Tipe Pokemon tidak tersedia");
                        }
                        Glide.with(Soal5Activity.this)
                                .load(pokemon.getImages().getLarge())
                                .into(imagePokemon);

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<CardResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });

        builder.setPositiveButton("Tutup", (dialog, which) -> {
            dialog.dismiss();
        });

        builder.setNeutralButton("Unduh Gambar", (dialog, which) -> {
            downloadPokemonImage(pokemon.getImages().getLarge());
            dialog.dismiss();
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void downloadPokemonImage(String imageUrl) {
        Glide.with(this)
                .asFile()
                .load(imageUrl)
                .into(new SimpleTarget<File>() {
                    @Override
                    public void onResourceReady(@NonNull File resource, Transition<? super File> transition) {
                        saveImageLocally(resource);
                    }
                });
    }

    private void saveImageLocally(File imageFile) {
        String localDirectoryPath = Environment.getExternalStorageDirectory().toString() + "/PokemonImages/";

        File localDirectory = new File(localDirectoryPath);
        if (!localDirectory.exists()) {
            localDirectory.mkdirs();
        }

        String uniqueFileName = "pokemon_image_" + System.currentTimeMillis() + ".jpg";

        File destinationFile = new File(localDirectory, uniqueFileName);

        try {
            FileInputStream inputStream = new FileInputStream(imageFile);
            FileOutputStream outputStream = new FileOutputStream(destinationFile);

            byte[] buffer = new byte[1024];
            int length;
            while ((length = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, length);
            }

            inputStream.close();
            outputStream.close();

            Toast.makeText(this, "Gambar Pokemon telah diunduh dan disimpan.", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Gagal menyimpan gambar Pokemon.", Toast.LENGTH_SHORT).show();
        }
    }

}