package com.trionohidayat.jari.soal5.api;

import com.trionohidayat.jari.soal5.response.CardResponse;
import com.trionohidayat.jari.soal5.response.PokemonResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @GET("cards")
    Call<PokemonResponse> searchCards(
            @Query("q") String query,
            @Query("page") int page,
            @Query("pageSize") int pageSize
    );

    @GET("cards/{id}")
    Call<CardResponse> getCards(
            @Path("id") String id
    );
}
