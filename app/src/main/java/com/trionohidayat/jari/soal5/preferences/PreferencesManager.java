package com.trionohidayat.jari.soal5.preferences;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferencesManager {
    private static final String PREF_NAME = "Soal5Preferences";
    private static final String KEY_IS_CONNECTED_TO_INTERNET = "IsConnectedToInternet";

    private final SharedPreferences sharedPreferences;

    public PreferencesManager(Context context) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public boolean isConnectedToInternet() {
        return sharedPreferences.getBoolean(KEY_IS_CONNECTED_TO_INTERNET, false);
    }

    public void setConnectedToInternet(boolean isConnected) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(KEY_IS_CONNECTED_TO_INTERNET, isConnected);
        editor.apply();
    }
}

