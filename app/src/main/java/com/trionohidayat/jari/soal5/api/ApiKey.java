package com.trionohidayat.jari.soal5.api;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class ApiKey implements Interceptor {
    private static final String API_KEY = "968a001d-0440-47cc-8fff-b09f02c9862c";

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request originalRequest = chain.request();
        Request newRequest = originalRequest.newBuilder()
                .header("X-Api-Key", API_KEY)
                .build();

        return chain.proceed(newRequest);
    }
}