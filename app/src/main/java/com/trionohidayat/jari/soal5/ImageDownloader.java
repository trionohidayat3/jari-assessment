package com.trionohidayat.jari.soal5;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.trionohidayat.jari.soal5.database.PokemonDatabase;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ImageDownloader {

    private final Context context;

    public ImageDownloader(Context context) {
        this.context = context;
    }

    public void downloadAndSaveImage(String imageUrl, String pokemonId, String pokemonName) {
        // Tentukan direktori penyimpanan gambar
        File storageDir = context.getExternalFilesDir("pokemon_images");
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }

        // Tentukan path file gambar yang akan disimpan
        File imageFile = new File(storageDir, pokemonId + ".jpg");

        // Gunakan Glide untuk mengunduh dan menyimpan gambar
        Glide.with(context)
                .load(imageUrl)
                .apply(new RequestOptions().override(Target.SIZE_ORIGINAL))
                .into(new CustomTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        // Simpan gambar ke perangkat
                        try {
                            FileOutputStream fos = new FileOutputStream(imageFile);
                            Bitmap bitmap = ((BitmapDrawable) resource).getBitmap();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                            fos.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        // Simpan jalur gambar ke dalam database SQLite
                        PokemonDatabase dbHelper = new PokemonDatabase(context);
                        dbHelper.savePokemonImage(pokemonId, pokemonName, imageFile.getAbsolutePath());
                        dbHelper.close();
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
                        // Handle jika gambar gagal diunduh
                    }
                });
    }
}

