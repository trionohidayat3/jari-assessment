package com.trionohidayat.jari.soal5.model;

import java.util.List;

public class Pokemon {
    public String id, name, supertype, hp, evolvesFrom, number, artist, rarity, flavorText, imagePath;
    public List<String> subtypes, types, retreatCost;
    public List<Attack> attacks;
    public List<Weakness> weaknesses;
    public List<Resistance> resistances;
    public int convertedRetreatCost;
    public CardSet set;
    public List<Integer> nationalPokedexNumbers;
    public Legalities legalities;
    public Images images;
    public TCGPlayer tcgplayer;
    public CardMarket cardmarket;

    public Pokemon(String id, String name, String imagePath) {
        this.id = id;
        this.name = name;
        this.imagePath = imagePath;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSupertype() {
        return supertype;
    }

    public void setSupertype(String supertype) {
        this.supertype = supertype;
    }

    public List<String> getSubtypes() {
        return subtypes;
    }

    public void setSubtypes(List<String> subtypes) {
        this.subtypes = subtypes;
    }

    public String getHp() {
        return hp;
    }

    public void setHp(String hp) {
        this.hp = hp;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public String getEvolvesFrom() {
        return evolvesFrom;
    }

    public void setEvolvesFrom(String evolvesFrom) {
        this.evolvesFrom = evolvesFrom;
    }

    public List<Attack> getAttacks() {
        return attacks;
    }

    public void setAttacks(List<Attack> attacks) {
        this.attacks = attacks;
    }

    public List<Weakness> getWeaknesses() {
        return weaknesses;
    }

    public void setWeaknesses(List<Weakness> weaknesses) {
        this.weaknesses = weaknesses;
    }

    public List<Resistance> getResistances() {
        return resistances;
    }

    public void setResistances(List<Resistance> resistances) {
        this.resistances = resistances;
    }

    public List<String> getRetreatCost() {
        return retreatCost;
    }

    public void setRetreatCost(List<String> retreatCost) {
        this.retreatCost = retreatCost;
    }

    public int getConvertedRetreatCost() {
        return convertedRetreatCost;
    }

    public void setConvertedRetreatCost(int convertedRetreatCost) {
        this.convertedRetreatCost = convertedRetreatCost;
    }

    public CardSet getSet() {
        return set;
    }

    public void setSet(CardSet set) {
        this.set = set;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getRarity() {
        return rarity;
    }

    public void setRarity(String rarity) {
        this.rarity = rarity;
    }

    public String getFlavorText() {
        return flavorText;
    }

    public void setFlavorText(String flavorText) {
        this.flavorText = flavorText;
    }

    public List<Integer> getNationalPokedexNumbers() {
        return nationalPokedexNumbers;
    }

    public void setNationalPokedexNumbers(List<Integer> nationalPokedexNumbers) {
        this.nationalPokedexNumbers = nationalPokedexNumbers;
    }

    public Legalities getLegalities() {
        return legalities;
    }

    public void setLegalities(Legalities legalities) {
        this.legalities = legalities;
    }

    public Images getImages() {
        return images;
    }

    public void setImages(Images images) {
        this.images = images;
    }

    public TCGPlayer getTcgplayer() {
        return tcgplayer;
    }

    public void setTcgplayer(TCGPlayer tcgplayer) {
        this.tcgplayer = tcgplayer;
    }

    public CardMarket getCardmarket() {
        return cardmarket;
    }

    public void setCardmarket(CardMarket cardmarket) {
        this.cardmarket = cardmarket;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public static class Attack {
        public String name, damage, text;
        public List<String> cost;
        public int convertedEnergyCost;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<String> getCost() {
            return cost;
        }

        public void setCost(List<String> cost) {
            this.cost = cost;
        }

        public int getConvertedEnergyCost() {
            return convertedEnergyCost;
        }

        public void setConvertedEnergyCost(int convertedEnergyCost) {
            this.convertedEnergyCost = convertedEnergyCost;
        }

        public String getDamage() {
            return damage;
        }

        public void setDamage(String damage) {
            this.damage = damage;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }

    public static class Weakness {
        public String type, value;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public static class Resistance {
        public String type, value;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public static class CardSet {
        public String id, name, series, ptcgoCode, releaseDate, updatedAt;
        public int printedTotal, total;
        public Legalities legalities;
        public Images images;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSeries() {
            return series;
        }

        public void setSeries(String series) {
            this.series = series;
        }

        public int getPrintedTotal() {
            return printedTotal;
        }

        public void setPrintedTotal(int printedTotal) {
            this.printedTotal = printedTotal;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public Legalities getLegalities() {
            return legalities;
        }

        public void setLegalities(Legalities legalities) {
            this.legalities = legalities;
        }

        public String getPtcgoCode() {
            return ptcgoCode;
        }

        public void setPtcgoCode(String ptcgoCode) {
            this.ptcgoCode = ptcgoCode;
        }

        public String getReleaseDate() {
            return releaseDate;
        }

        public void setReleaseDate(String releaseDate) {
            this.releaseDate = releaseDate;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Images getImages() {
            return images;
        }

        public void setImages(Images images) {
            this.images = images;
        }

        public static class Legalities {
            public String unlimited, expanded;

            public String getUnlimited() {
                return unlimited;
            }

            public void setUnlimited(String unlimited) {
                this.unlimited = unlimited;
            }

            public String getExpanded() {
                return expanded;
            }

            public void setExpanded(String expanded) {
                this.expanded = expanded;
            }
        }

        public static class Images {
            public String symbol, logo;

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }
        }
    }

    public static class Legalities {
        public String unlimited, expanded;

        public String getUnlimited() {
            return unlimited;
        }

        public void setUnlimited(String unlimited) {
            this.unlimited = unlimited;
        }

        public String getExpanded() {
            return expanded;
        }

        public void setExpanded(String expanded) {
            this.expanded = expanded;
        }
    }

    public static class Images {
        public String small, large;

        public String getSmall() {
            return small;
        }

        public void setSmall(String small) {
            this.small = small;
        }

        public String getLarge() {
            return large;
        }

        public void setLarge(String large) {
            this.large = large;
        }
    }

    public static class TCGPlayer {
        public String url, updatedAt;
        public Prices prices;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Prices getPrices() {
            return prices;
        }

        public void setPrices(Prices prices) {
            this.prices = prices;
        }

        public static class Prices {
            public Price holofoil;
            public Price reverseHolofoil;

            public Price getHolofoil() {
                return holofoil;
            }

            public void setHolofoil(Price holofoil) {
                this.holofoil = holofoil;
            }

            public Price getReverseHolofoil() {
                return reverseHolofoil;
            }

            public void setReverseHolofoil(Price reverseHolofoil) {
                this.reverseHolofoil = reverseHolofoil;
            }

            public static class Price {
                public double low;
                public double mid;
                public double high;
                public double market;
                public Double directLow;

                public double getLow() {
                    return low;
                }

                public void setLow(double low) {
                    this.low = low;
                }

                public double getMid() {
                    return mid;
                }

                public void setMid(double mid) {
                    this.mid = mid;
                }

                public double getHigh() {
                    return high;
                }

                public void setHigh(double high) {
                    this.high = high;
                }

                public double getMarket() {
                    return market;
                }

                public void setMarket(double market) {
                    this.market = market;
                }

                public Double getDirectLow() {
                    return directLow;
                }

                public void setDirectLow(Double directLow) {
                    this.directLow = directLow;
                }
            }
        }
    }

    public static class CardMarket {
        public String url, updatedAt;
        public Prices prices;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Prices getPrices() {
            return prices;
        }

        public void setPrices(Prices prices) {
            this.prices = prices;
        }

        public static class Prices {
            public double averageSellPrice, lowPrice, trendPrice, germanProLow, suggestedPrice, reverseHoloSell, reverseHoloLow,
                    reverseHoloTrend, lowPriceExPlus, avg1, avg7, avg30, reverseHoloAvg1, reverseHoloAvg7, reverseHoloAvg30;

            public double getAverageSellPrice() {
                return averageSellPrice;
            }

            public void setAverageSellPrice(double averageSellPrice) {
                this.averageSellPrice = averageSellPrice;
            }

            public double getLowPrice() {
                return lowPrice;
            }

            public void setLowPrice(double lowPrice) {
                this.lowPrice = lowPrice;
            }

            public double getTrendPrice() {
                return trendPrice;
            }

            public void setTrendPrice(double trendPrice) {
                this.trendPrice = trendPrice;
            }

            public double getGermanProLow() {
                return germanProLow;
            }

            public void setGermanProLow(double germanProLow) {
                this.germanProLow = germanProLow;
            }

            public double getSuggestedPrice() {
                return suggestedPrice;
            }

            public void setSuggestedPrice(double suggestedPrice) {
                this.suggestedPrice = suggestedPrice;
            }

            public double getReverseHoloSell() {
                return reverseHoloSell;
            }

            public void setReverseHoloSell(double reverseHoloSell) {
                this.reverseHoloSell = reverseHoloSell;
            }

            public double getReverseHoloLow() {
                return reverseHoloLow;
            }

            public void setReverseHoloLow(double reverseHoloLow) {
                this.reverseHoloLow = reverseHoloLow;
            }

            public double getReverseHoloTrend() {
                return reverseHoloTrend;
            }

            public void setReverseHoloTrend(double reverseHoloTrend) {
                this.reverseHoloTrend = reverseHoloTrend;
            }

            public double getLowPriceExPlus() {
                return lowPriceExPlus;
            }

            public void setLowPriceExPlus(double lowPriceExPlus) {
                this.lowPriceExPlus = lowPriceExPlus;
            }

            public double getAvg1() {
                return avg1;
            }

            public void setAvg1(double avg1) {
                this.avg1 = avg1;
            }

            public double getAvg7() {
                return avg7;
            }

            public void setAvg7(double avg7) {
                this.avg7 = avg7;
            }

            public double getAvg30() {
                return avg30;
            }

            public void setAvg30(double avg30) {
                this.avg30 = avg30;
            }

            public double getReverseHoloAvg1() {
                return reverseHoloAvg1;
            }

            public void setReverseHoloAvg1(double reverseHoloAvg1) {
                this.reverseHoloAvg1 = reverseHoloAvg1;
            }

            public double getReverseHoloAvg7() {
                return reverseHoloAvg7;
            }

            public void setReverseHoloAvg7(double reverseHoloAvg7) {
                this.reverseHoloAvg7 = reverseHoloAvg7;
            }

            public double getReverseHoloAvg30() {
                return reverseHoloAvg30;
            }

            public void setReverseHoloAvg30(double reverseHoloAvg30) {
                this.reverseHoloAvg30 = reverseHoloAvg30;
            }
        }
    }
}

