package com.trionohidayat.jari.soal5.response;

import java.util.List;

public class CardResponse {
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        private String id;
        private String name;
        private String supertype;
        private List<String> subtypes;
        private String hp;
        private List<String> types;
        private String evolvesFrom;
        private List<Attack> attacks;
        private List<Weakness> weaknesses;
        private List<Resistance> resistances;
        private List<String> retreatCost;
        private int convertedRetreatCost;
        private Set set;
        private String number;
        private String artist;
        private String rarity;
        private String flavorText;
        private List<Integer> nationalPokedexNumbers;
        private Legalities legalities;
        private Images images;
        private Tcgplayer tcgplayer;
        private Cardmarket cardmarket;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSupertype() {
            return supertype;
        }

        public void setSupertype(String supertype) {
            this.supertype = supertype;
        }

        public List<String> getSubtypes() {
            return subtypes;
        }

        public void setSubtypes(List<String> subtypes) {
            this.subtypes = subtypes;
        }

        public String getHp() {
            return hp;
        }

        public void setHp(String hp) {
            this.hp = hp;
        }

        public List<String> getTypes() {
            return types;
        }

        public void setTypes(List<String> types) {
            this.types = types;
        }

        public String getEvolvesFrom() {
            return evolvesFrom;
        }

        public void setEvolvesFrom(String evolvesFrom) {
            this.evolvesFrom = evolvesFrom;
        }

        public List<Attack> getAttacks() {
            return attacks;
        }

        public void setAttacks(List<Attack> attacks) {
            this.attacks = attacks;
        }

        public List<Weakness> getWeaknesses() {
            return weaknesses;
        }

        public void setWeaknesses(List<Weakness> weaknesses) {
            this.weaknesses = weaknesses;
        }

        public List<Resistance> getResistances() {
            return resistances;
        }

        public void setResistances(List<Resistance> resistances) {
            this.resistances = resistances;
        }

        public List<String> getRetreatCost() {
            return retreatCost;
        }

        public void setRetreatCost(List<String> retreatCost) {
            this.retreatCost = retreatCost;
        }

        public int getConvertedRetreatCost() {
            return convertedRetreatCost;
        }

        public void setConvertedRetreatCost(int convertedRetreatCost) {
            this.convertedRetreatCost = convertedRetreatCost;
        }

        public Set getSet() {
            return set;
        }

        public void setSet(Set set) {
            this.set = set;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getArtist() {
            return artist;
        }

        public void setArtist(String artist) {
            this.artist = artist;
        }

        public String getRarity() {
            return rarity;
        }

        public void setRarity(String rarity) {
            this.rarity = rarity;
        }

        public String getFlavorText() {
            return flavorText;
        }

        public void setFlavorText(String flavorText) {
            this.flavorText = flavorText;
        }

        public List<Integer> getNationalPokedexNumbers() {
            return nationalPokedexNumbers;
        }

        public void setNationalPokedexNumbers(List<Integer> nationalPokedexNumbers) {
            this.nationalPokedexNumbers = nationalPokedexNumbers;
        }

        public Legalities getLegalities() {
            return legalities;
        }

        public void setLegalities(Legalities legalities) {
            this.legalities = legalities;
        }

        public Images getImages() {
            return images;
        }

        public void setImages(Images images) {
            this.images = images;
        }

        public Tcgplayer getTcgplayer() {
            return tcgplayer;
        }

        public void setTcgplayer(Tcgplayer tcgplayer) {
            this.tcgplayer = tcgplayer;
        }

        public Cardmarket getCardmarket() {
            return cardmarket;
        }

        public void setCardmarket(Cardmarket cardmarket) {
            this.cardmarket = cardmarket;
        }

        public static class Attack {
            private String name;
            private List<String> cost;
            private int convertedEnergyCost;
            private String damage;
            private String text;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public List<String> getCost() {
                return cost;
            }

            public void setCost(List<String> cost) {
                this.cost = cost;
            }

            public int getConvertedEnergyCost() {
                return convertedEnergyCost;
            }

            public void setConvertedEnergyCost(int convertedEnergyCost) {
                this.convertedEnergyCost = convertedEnergyCost;
            }

            public String getDamage() {
                return damage;
            }

            public void setDamage(String damage) {
                this.damage = damage;
            }

            public String getText() {
                return text;
            }

            public void setText(String text) {
                this.text = text;
            }
        }

        public static class Weakness {
            private String type;
            private String value;

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }
        }

        public static class Resistance {
            private String type;
            private String value;

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }
        }

        public static class Set {
            private String id;
            private String name;
            private String series;
            private int printedTotal;
            private int total;
            private Legalities legalities;
            private String ptcgoCode;
            private String releaseDate;
            private String updatedAt;
            private Images images;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSeries() {
                return series;
            }

            public void setSeries(String series) {
                this.series = series;
            }

            public int getPrintedTotal() {
                return printedTotal;
            }

            public void setPrintedTotal(int printedTotal) {
                this.printedTotal = printedTotal;
            }

            public int getTotal() {
                return total;
            }

            public void setTotal(int total) {
                this.total = total;
            }

            public Legalities getLegalities() {
                return legalities;
            }

            public void setLegalities(Legalities legalities) {
                this.legalities = legalities;
            }

            public String getPtcgoCode() {
                return ptcgoCode;
            }

            public void setPtcgoCode(String ptcgoCode) {
                this.ptcgoCode = ptcgoCode;
            }

            public String getReleaseDate() {
                return releaseDate;
            }

            public void setReleaseDate(String releaseDate) {
                this.releaseDate = releaseDate;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public Images getImages() {
                return images;
            }

            public void setImages(Images images) {
                this.images = images;
            }
        }

        public static class Legalities {
            private String unlimited;

            public String getUnlimited() {
                return unlimited;
            }

            public void setUnlimited(String unlimited) {
                this.unlimited = unlimited;
            }
        }

        public static class Images {
            private String symbol;
            private String logo;
            private String small;
            private String large;

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }

            public String getSmall() {
                return small;
            }

            public void setSmall(String small) {
                this.small = small;
            }

            public String getLarge() {
                return large;
            }

            public void setLarge(String large) {
                this.large = large;
            }
        }

        public static class Tcgplayer {
            private String url;
            private String updatedAt;
            private Prices prices;

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public Prices getPrices() {
                return prices;
            }

            public void setPrices(Prices prices) {
                this.prices = prices;
            }

            public static class Prices {
                private Price holofoil;
                private Price reverseHolofoil;

                public Price getHolofoil() {
                    return holofoil;
                }

                public void setHolofoil(Price holofoil) {
                    this.holofoil = holofoil;
                }

                public Price getReverseHolofoil() {
                    return reverseHolofoil;
                }

                public void setReverseHolofoil(Price reverseHolofoil) {
                    this.reverseHolofoil = reverseHolofoil;
                }

                public static class Price {
                    private double low;
                    private double mid;
                    private double high;
                    private double market;
                    private Double directLow;

                    public double getLow() {
                        return low;
                    }

                    public void setLow(double low) {
                        this.low = low;
                    }

                    public double getMid() {
                        return mid;
                    }

                    public void setMid(double mid) {
                        this.mid = mid;
                    }

                    public double getHigh() {
                        return high;
                    }

                    public void setHigh(double high) {
                        this.high = high;
                    }

                    public double getMarket() {
                        return market;
                    }

                    public void setMarket(double market) {
                        this.market = market;
                    }

                    public Double getDirectLow() {
                        return directLow;
                    }

                    public void setDirectLow(Double directLow) {
                        this.directLow = directLow;
                    }
                }
            }
        }

        public static class Cardmarket {
            private String url;
            private String updatedAt;
            private Prices prices;

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public Prices getPrices() {
                return prices;
            }

            public void setPrices(Prices prices) {
                this.prices = prices;
            }

            public static class Prices {
                private double averageSellPrice;
                private double lowPrice;
                private double trendPrice;
                private double germanProLow;
                private double suggestedPrice;
                private double reverseHoloSell;
                private double reverseHoloLow;
                private double reverseHoloTrend;
                private double lowPriceExPlus;
                private double avg1;
                private double avg7;
                private double avg30;
                private double reverseHoloAvg1;
                private double reverseHoloAvg7;
                private double reverseHoloAvg30;

                public double getAverageSellPrice() {
                    return averageSellPrice;
                }

                public void setAverageSellPrice(double averageSellPrice) {
                    this.averageSellPrice = averageSellPrice;
                }

                public double getLowPrice() {
                    return lowPrice;
                }

                public void setLowPrice(double lowPrice) {
                    this.lowPrice = lowPrice;
                }

                public double getTrendPrice() {
                    return trendPrice;
                }

                public void setTrendPrice(double trendPrice) {
                    this.trendPrice = trendPrice;
                }

                public double getGermanProLow() {
                    return germanProLow;
                }

                public void setGermanProLow(double germanProLow) {
                    this.germanProLow = germanProLow;
                }

                public double getSuggestedPrice() {
                    return suggestedPrice;
                }

                public void setSuggestedPrice(double suggestedPrice) {
                    this.suggestedPrice = suggestedPrice;
                }

                public double getReverseHoloSell() {
                    return reverseHoloSell;
                }

                public void setReverseHoloSell(double reverseHoloSell) {
                    this.reverseHoloSell = reverseHoloSell;
                }

                public double getReverseHoloLow() {
                    return reverseHoloLow;
                }

                public void setReverseHoloLow(double reverseHoloLow) {
                    this.reverseHoloLow = reverseHoloLow;
                }

                public double getReverseHoloTrend() {
                    return reverseHoloTrend;
                }

                public void setReverseHoloTrend(double reverseHoloTrend) {
                    this.reverseHoloTrend = reverseHoloTrend;
                }

                public double getLowPriceExPlus() {
                    return lowPriceExPlus;
                }

                public void setLowPriceExPlus(double lowPriceExPlus) {
                    this.lowPriceExPlus = lowPriceExPlus;
                }

                public double getAvg1() {
                    return avg1;
                }

                public void setAvg1(double avg1) {
                    this.avg1 = avg1;
                }

                public double getAvg7() {
                    return avg7;
                }

                public void setAvg7(double avg7) {
                    this.avg7 = avg7;
                }

                public double getAvg30() {
                    return avg30;
                }

                public void setAvg30(double avg30) {
                    this.avg30 = avg30;
                }

                public double getReverseHoloAvg1() {
                    return reverseHoloAvg1;
                }

                public void setReverseHoloAvg1(double reverseHoloAvg1) {
                    this.reverseHoloAvg1 = reverseHoloAvg1;
                }

                public double getReverseHoloAvg7() {
                    return reverseHoloAvg7;
                }

                public void setReverseHoloAvg7(double reverseHoloAvg7) {
                    this.reverseHoloAvg7 = reverseHoloAvg7;
                }

                public double getReverseHoloAvg30() {
                    return reverseHoloAvg30;
                }

                public void setReverseHoloAvg30(double reverseHoloAvg30) {
                    this.reverseHoloAvg30 = reverseHoloAvg30;
                }
            }
        }
    }
}

