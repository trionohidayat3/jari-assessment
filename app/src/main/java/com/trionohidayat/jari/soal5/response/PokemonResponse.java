package com.trionohidayat.jari.soal5.response;

import com.trionohidayat.jari.soal5.model.Pokemon;

import java.util.List;

public class PokemonResponse {
    public List<Pokemon> data;

    public List<Pokemon> getData() {
        return data;
    }

    public void setData(List<Pokemon> data) {
        this.data = data;
    }
}
