package com.trionohidayat.jari.soal5.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.trionohidayat.jari.R;
import com.trionohidayat.jari.soal5.ItemClickListener;
import com.trionohidayat.jari.soal5.database.PokemonDatabase;
import com.trionohidayat.jari.soal5.model.Pokemon;

import java.io.File;
import java.util.List;

public class PokemonAdapter extends RecyclerView.Adapter<PokemonAdapter.ViewHolder> {
    private final List<Pokemon> cards;
    private final Context context;
    private final boolean isConnectedToInternet;
    private final PokemonDatabase dbHelper;

    private ItemClickListener itemClickListener;

    public void setItemClickListener(ItemClickListener listener) {
        this.itemClickListener = listener;
    }


    public PokemonAdapter(List<Pokemon> cards, Context context, boolean isConnectedToInternet) {
        this.cards = cards;
        this.context = context;
        this.isConnectedToInternet = isConnectedToInternet;
        this.dbHelper = new PokemonDatabase(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Pokemon card = cards.get(position);

        holder.cardNameTextView.setText(card.getName());

        if (isConnectedToInternet) {
            Glide.with(context)
                    .load(card.getImages().getSmall())
                    .into(holder.cardImageView);
        } else {
            String imagePath = dbHelper.getPokemonImagePath(card.getId());
            if (imagePath != null) {
                Glide.with(context)
                        .load(new File(imagePath))
                        .into(holder.cardImageView);
            }
        }

        holder.itemView.setOnClickListener(view -> {
            if (itemClickListener != null) {
                itemClickListener.onItemClick(card);
            }
        });
    }

    @Override
    public int getItemCount() {
        return cards.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final ImageView cardImageView;
        private final TextView cardNameTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            cardImageView = itemView.findViewById(R.id.imageCard);
            cardNameTextView = itemView.findViewById(R.id.textName);
        }
    }

    public void setCards(List<Pokemon> newCards) {
        this.cards.clear();
        this.cards.addAll(newCards);
        notifyDataSetChanged();
    }

    public void addCards(List<Pokemon> newCards) {
        int oldSize = this.cards.size();
        this.cards.addAll(newCards);
        notifyItemRangeInserted(oldSize, newCards.size());
    }
}


