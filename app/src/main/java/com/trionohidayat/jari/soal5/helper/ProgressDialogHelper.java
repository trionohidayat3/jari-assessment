package com.trionohidayat.jari.soal5.helper;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.trionohidayat.jari.R;

public class ProgressDialogHelper {

    private final AlertDialog progressDialog;

    public ProgressDialogHelper(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_progress, null);
        progressDialog = builder.setView(dialogView).create();
        progressDialog.setCancelable(false);
    }

    public void show() {
        progressDialog.show();
    }

    public void hide() {
        progressDialog.dismiss();
    }
}

