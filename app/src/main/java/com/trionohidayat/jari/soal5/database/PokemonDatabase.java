package com.trionohidayat.jari.soal5.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.trionohidayat.jari.soal5.model.Pokemon;

import java.util.ArrayList;
import java.util.List;

public class PokemonDatabase extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "pokemon.db";
    private static final int DATABASE_VERSION = 1;

    private static final String TABLE_POKEMON = "pokemon";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_IMAGE_PATH = "image_path";

    private static final String TABLE_CREATE =
            "CREATE TABLE " + TABLE_POKEMON + " (" +
                    COLUMN_ID + " TEXT PRIMARY KEY, " +
                    COLUMN_NAME + " TEXT, " +
                    COLUMN_IMAGE_PATH + " TEXT);";

    public PokemonDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_POKEMON);
        onCreate(db);
    }

    public void savePokemonImage(String pokemonId, String pokemonName, String imagePath) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_ID, pokemonId);
        values.put(COLUMN_NAME, pokemonName);
        values.put(COLUMN_IMAGE_PATH, imagePath);

        database.replace(TABLE_POKEMON, null, values);
        database.close();
    }

    public List<Pokemon> loadPokemonDataFromSQLite() {
        List<Pokemon> pokemonList = new ArrayList<>();
        SQLiteDatabase database = this.getReadableDatabase();

        Cursor cursor = database.query(
                TABLE_POKEMON,
                null,
                null,
                null,
                null,
                null,
                null
        );

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    String id = cursor.getString(0);
                    String name = cursor.getString(1);
                    String imagePath = cursor.getString(2);
                    Pokemon pokemon = new Pokemon(id, name, imagePath);
                    pokemonList.add(pokemon);
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        database.close();

        return pokemonList;
    }

    public String getPokemonImagePath(String pokemonId) {
        SQLiteDatabase database = this.getReadableDatabase();
        String imagePath = null;

        Cursor cursor = database.query(
                TABLE_POKEMON,
                new String[]{COLUMN_IMAGE_PATH},
                COLUMN_ID + "=?",
                new String[]{pokemonId},
                null,
                null,
                null
        );

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                imagePath = cursor.getString(cursor.getColumnIndex(COLUMN_IMAGE_PATH));
            }
            cursor.close();
        }
        database.close();

        return imagePath;
    }

}
