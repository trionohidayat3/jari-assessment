package com.trionohidayat.jari.soal4;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.appbar.MaterialToolbar;
import com.trionohidayat.jari.R;
import com.trionohidayat.jari.soal4.adapter.PagerAdapter;
import com.trionohidayat.jari.databinding.ActivitySoal4Binding;

public class Soal4Activity extends AppCompatActivity {

    ActivitySoal4Binding binding;

    MaterialToolbar toolbar;
    private ViewPager2 viewPager2;
    String[] questions, answers;
    private int currentSlide = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySoal4Binding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        toolbar = binding.toolBar;

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        viewPager2 = binding.viewPager;

        questions = getResources().getStringArray(R.array.questions);
        answers = getResources().getStringArray(R.array.answers);

        PagerAdapter adapter = new PagerAdapter(this, questions, answers);
        viewPager2.setAdapter(adapter);

        updateTotalSlides();

        binding.buttonPrevious.setOnClickListener(v -> {
            if (currentSlide > 0) {
                currentSlide--;
                viewPager2.setCurrentItem(currentSlide);
                updateTotalSlides();
            }
        });

        binding.buttonNext.setOnClickListener(v -> {
            if (currentSlide < questions.length - 1) {
                currentSlide++;
                viewPager2.setCurrentItem(currentSlide);
                updateTotalSlides();
            }
        });
    }

    private void updateTotalSlides() {
        int totalSlides = questions.length;
        currentSlide = Math.min(currentSlide, totalSlides - 1);
        binding.textTotalSlide.setText((currentSlide + 1) + "/" + totalSlides);
        binding.buttonPrevious.setEnabled(currentSlide > 0);
        binding.buttonNext.setEnabled(currentSlide < totalSlides - 1);
    }
}