package com.trionohidayat.jari.soal4.model;

public class qna {
    private String question, answer;

    public qna(String question, String answer) {
        this.question = question;
        this.answer = answer;
    }

    public String getQuestion() {
        return question;
    }

    public String getAnswer() {
        return answer;
    }
}

