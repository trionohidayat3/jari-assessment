package com.trionohidayat.jari.soal4.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.trionohidayat.jari.R;

public class PagerAdapter extends RecyclerView.Adapter<PagerAdapter.ViewHolder> {

    private final Context context;
    private final String[] questions, answers;

    public PagerAdapter(Context context, String[] questions, String[] answers) {
        this.context = context;
        this.questions = questions;
        this.answers = answers;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.pager_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textQuestion.setText(questions[position]);
        holder.textAnswer.setText(answers[position]);
    }

    @Override
    public int getItemCount() {
        return questions.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView textQuestion, textAnswer;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textQuestion = itemView.findViewById(R.id.textQuestion);
            textAnswer = itemView.findViewById(R.id.textAnswer);
        }
    }
}
